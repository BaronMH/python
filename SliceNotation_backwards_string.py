# Slice Notation

# a[start:stop:step] # start through not past stop, by step
# a[start:stop]  # items start through stop-1
# a[start:]      # items start through the rest of the array
# a[:stop]       # items from the beginning through stop-1
# a[:]           # a copy of the whole array


word = "Hello"

print(word[::-1])
